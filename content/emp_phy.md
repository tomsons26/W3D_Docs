###### W3D in Max > Emitters > The W3D Emitter System > Properties - Physics
# Emitter Properties - Physics
***
![](images/scrnshots/em_physics.gif)

**Velocity:**

**![](images/note_sm.gif)Note:** The Velocity properties are based on the emitter's **LOCAL** axis.  
Set the starting velocity of the particles based on their local axis. The higher the number the more velocity it has in that direction.

**X:**

Set the local X particle velocity values.

**Y:**

Set the local Y particle velocity values.

**Z:**

Set the local Z particle velocity values.

**Additive Velocity Factors:**

More adjustable velocity properties.

**Outward**

Adjust how much it moves outward from it's origin.  
![](images/note_sm.gif) **NOTE:** Outward will not work if the creation volume is set at "0"

**Inheritance:**

Use this to set the particles Inherit velocity. The particles inherit the velocity of the emitter's motion, at that moment in time. The particles will **not** follow the emitter.

**Acceleration:**

Adjust the particles acceleration. ( Based on a global values). The higher the number the more it accelerates in that direction.

**X:**

Set the X value of the particles acceleration.

**Y:**

Set the Y value of the particles acceleration.

**Z:**

Set the Z value of the particles acceleration.

**Randomizer:**

Similar to Outward but more chaotic.

![](images/scrnshots/vol_randomizer.gif)

****Box:****

Set the Volume of the Randomizer to the shape of a box.

**X**

Set the Randomizer's width.

**Y**

Set the Randomizer's depth.

**Z**

Set the Randomizer's height.

**Sphere:**

Set the volume of the Randomizer to the shape of a sphere.

**Radius**

Set the radius size of the Randomizer.

**Hollow**

Selecting this forces the particle randomizer to use only the outer circumference of the sphere, no particles will be emitted from the inner volume.

**Cylinder:**

Set the Randomizer to the shape of a cylinder.

**Radius**

Set the radius of the Cylindrical.

**Height**

Specify the Height of the cylinder.

![](images/note_sm.gif)**NOTE:** If you're looking for a "Speed" value, there isn't one. That's because It's the velocity and acceleration values that determine the speed.  Adjust these to determine the speed at which the particle travel.  

***

**END**

[Back to top](#top)