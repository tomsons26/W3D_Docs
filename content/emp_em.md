###### W3D in Max > Emitters > The W3D Emitter System > Properties - Emission
# Emitter Properties - Emission
***
![](images/scrnshots/em_emission.gif)

**Bursts:**

Each time a particle or set number of particles is emitted from the emitter at one time.

**Rate**

Type in a numerical value or adjust the spinner to set the rate of bursts per second.

**Particles**

Sets the number of particles that will initially be created at the same time. Example: A setting of 3 will create 3 bursts or (particles) simultaneously.

**Cap:**

Check mark this box to enable the total amount of particles to be displayed.

**Max Particles**

Use the spinner or type in a numerical value to set the total amount of particles that will be displayed. This overrides any lifespan settings.

**Creation Volume:**

-(see below)

**Specify...**

Clicking this opens up the Volume Randomizer, which allows you to customize the emitters shape and size.

**![](images/note_sm.gif)NOTE: In many cases, the creation volume doesn't like you to use a "0" value,  
Try to use minimal values other than just "0", for best results.**

**Volume Randomizer:**

Allows you to adjust the shape and size of the where the particles will emit from.

![](images/scrnshots/vol_randomizer.gif)

**Box:**

Set the emitter to be in the shape of a box.

**X**

Specify the Width ( X ) of the box.

**Y**

Specify the Length or Depth ( Y ) of the box.

**Z**

Specify the Height ( Z ) of the box.

****Sphere:****

Set the emitter to be in the shape of a sphere.

**Radius**

Set the radius of the Spherical emitter

**Hollow**

Selecting this forces the particles to emit from only the outer perimeter of the sphere, no particles will be emitted from inside the sphere.

**Cylinder:**

Set the emitter to be in the shape of a cylinder.

**Radius**

Set the radius of the Cylindrical emitter

**Height**

Specify the Height of the cylinder.

  

***

**END**

[Back to top](#top)