###### W3D in Max > Emitters > The W3D Emitter System > Properties - General
# Emitter General Properties
***
![](images/scrnshots/em_general.gif)

**Emitter Name:**

Name your emitter in the space provided

**Particle Lifetime:**

Check mark this to activate it then type in a numerical value or use the spinner to set the lifetime of the particles in seconds.

![](images/note.jpg)The particle lifetime and it's size are relative, Adjusting one may affect the other in many cases.

**Shader:**

Blending options for various particle display.

**Additive**

This is the default mode. Looks at the color information in each channel and brightens the base color to reflect the colors behind it, creating the effect of an additive overlay.

**Alpha**

The map or color in its normal state, when using a texture map it uses its own alpha channel to set the transparency of it.

**Alpha - Test**

Adjusts the threshold of the alpha channel to make it higher in contrast, no grays just black and white, resulting in harsher edges in the transparency.

**Alpha- Test- Blend**

Same as above except it also blends the original alpha channel transparency.

**Multiplicative**

Looks at the color information in each channel and multiplies the base color by the blend color. The result color is always a darker color. Multiplying any color with black produces black.

**Opaque**

The map or color in its normal state with not using the alpha channel.

**Screen**

Similar to additive but doesn't burn as much. Creates a transparency based on the grayscale values of the map.  A little more costly than additive.

**Rendering Mode:**

 

**Triangle Particles**

Particles are in shape of a triangle - Less taxing than Quad particles

**Quad Particles**

Particles are in the shape of a Square

**Line**

Particles are in the shape of a line

**Tetrahedron Line Group**

Particles are in the shape of a pyramid.

**Prism Line Group**

Particles are in the shape of two triangles, connected together.

**Texture Filename:**

Here you can browse for your textures to map on to the particles of the emitter. (.Tga only)

  

***

**END**

[Back to top](#top)