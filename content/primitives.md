###### W3D in Max > Primitives
# The W3D primitives
***

#### Overview
**1\. W3D Primitives [General information](#geninfo)**

**2\. W3D Primitives-[Sphere](prim_sphere_gen.md)**

**3\. W3D Primitives-[Ring](prim_ring_gen.md)**

***

There are two different kinds of primitives you can create in W3D, a sphere and a ring. They can be animated to scale in size the the color and opacity can be animated as well.

**To Create a New Primitive:**  
Go to the Main Menu under Primitives>Create Primitive

The Primitive Properties box will pop open.  
![](images/idea_sm.gif) It is a good idea to name your emitter before clicking the "OK" button and settling for the default name.

 **![](images/note_sm.gif) NOTE:**  If you close the file or program, it will not ask you if you'd like to save your work, it will just close the program. You can avoid costly mistakes by ![](images/scrnshots/ex_prim_button.gif) Exporting often. This saves your primitive so you can reopen it later.  Naming the primitive and exporting it are two different things, make sure to do both.  

**![](images/scrnshots/primitives_hier.gif)**

![](images/point.jpg)Once you have named it, You can access it through the asset display window in it's hierarchy rollout under "Primitives" just click the name of your primitive to select it.  
  
Once Selected you can edit it's properties by going to the main menu under Primitives> Edit Primitive.  
![](images/idea_sm.gif)The easier way to access the properties panel is to select the primitive and hit the "Enter" key on your keyboard.

Primitives are based on their local coordinate system as shown below:

![](images/scrnshots/local_axis.gif)

**local axis Coordinates:**  
**Z = Height**  
**Y = Width**  
**X = Depth**  

  

![](images/scrnshots/primitive.jpg)

![](images/note_sm.gif)**NOTE:** You can type in negative (-) numerical values in most of the input fields of the primitive properties.  To do so, just type the minus key in before typing a numerical value.

![](images/note_sm.gif)**NOTE:** There is **NO** edit > undo for W3D.  When adjusting the properties of the primitive, It will save you some time to remember the values of what your about to adjust in case you make a mistake you can at least get back to the state at which it previously was.

***

**END**

[Back to top](#top)