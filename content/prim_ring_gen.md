###### Primitives > Ring > General
# Primitives - Ring properties - General
***
![](images/note_sm.gif)**NOTE: When you first create a ring primitive it wont be viewable**, that's because the camera is aligned to the front viewport by default and the ring primitive has "0" height.  To view the ring primitive, move the camera around a little by clicking and dragging in the viewport until you get a different angle.

![](images/scrnshots/prim_ring_gen.gif)

**General:**

You can adjust the general properties of the primitives you create through this menu.

**Name**

Name your ring primitive here.

**Duration**

Adjust the lifespan of the primitive.

**Camera Aligned**

Camera will always be frontally aligned to the primitive.

**Looping**

Select this to enable the lifespan of the primitive to loop.

**Shader:**

Select the way you wish to have the map or color of your primitive blend against any background objects or maps.

**Additive**

Looks at the color information in each channel and brightens the base color to reflect the colors behind it, creating the effect of an additive overlay, usually brightening and giving an "overexposed" effect.

**Alpha**

The map or color in its normal state. Takes the grayscale values and makes the darker colors more transparent and the lighter, more opaque.

**Multiplicative**

Looks at the color information in each channel and multiplies the base color by the blend color. The result color is always a darker color. Multiplying any color with black produces black.

**Opaque**

The map or color in its normal state without any transparency.

**Texture Filename:**

By clicking the "Browse" button, you can select a bitmap (.Tga only) you'd like to use for the primitive.

**Texture Tiling:**

This lets you tile the bitmap radially around the primitives center, to the amount of times you specify.

  

***

**END**

[Back to top](#top)