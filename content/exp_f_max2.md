###### Exporting from 3DS max to W3D part 2
# Viewing your files in W3D
***
#### Overview

**1\. [3 Step Process](#Process)**

**2\. [Exporting your bones](#bones)**

**3\. [Exporting your mesh](#mesh)**

**4\. [Exporting your animation](#anim1)(fish_swim)**

**5\. [Exporting your animation](#anim2)(fish_jump)**

***
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  
**3 Step Process**  
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  

For this process you have export them in the following order:

1\. Export the Bones  
2\. Export the Mesh  
3\. Export the Animation

![](images/note.jpg)These files must be in the same directory as the map.  Total size of file name including extension can't exceed 15 characters.

To begin this process we open the file "Mean\_fish\_swim" and go to the Utilities panel of max and under "more" look for "W3D tools". This will expand the W3D Export settings rollout.

![](images/scrnshots/1n.gif)  
![](images/idea_sm.gif) It is a good Idea to configure this to your default set under the utilities panel.

***

  
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  
**Exporting your bones**  
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

You can either use the ![select bones](images/scrnshots/select_bones_button.gif) button or manually select the controlling dummys in the scene.

![](images/scrnshots/exp_trans_bone.gif)

Leave the "Export Transform (bone)" checked.

Go "File > Export > "from the main menu, and name the file accordingly for bones, i.e.: "Fish_Bones".  
  
Save as file type ".W3d", make sure your saving them all to the same directory and click "Save". A dialog box appears, Click the "Skeleton" radio button and click "OK". (you can check review log if you like, It just lets you know what was exported).

***

  
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  
**Exporting your Mesh**  
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

Select your model, Leave "export transform" unchecked, and "Export Geometry" will remain checked.

Click the "Export with Std Mtls" and name the file accordingly for the model, i.e.: "Fish_model".

Save as file type ".W3d" and click "Save". The dialog box appears again, this time Click the "Hierarchical model" radio button.  

![](images/scrnshots/1q.gif)
  
Select the "Export Using Existing Skeleton" option, Click the button and select the bones to be used from the list, in this case -"Fish_bones.w3d", Click "open" the dialog box returns, click "OK".

![](images/scrnshots/exp_using_skel.gif)

This Exported the Model with the bones hierarchy in it.  

***

  
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  
**Exporting your animation (fish_swim)**  
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

Select just the bones, Click the "Export with Std Mtls" and name the file describing the animation, i.e.: "Fish_swim".

Save as file type ".W3d" and click "Save". When the dialog box pops open, select the "Pure Animation" radio button, and check the box "Export Using Existing Skeleton". Select the name of your bones file, i.e.: "fish_bones.w3d" (if not already selected. Make sure to select the number of frames your animation runs and click "OK" to Export.  

![](images/scrnshots/1r.gif)  

***

  
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  
**Exporting your animation (fish_jump)**  
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

Open the max file, "Mean\_fish\_jump", go to the utilities panel and click on W3D tools.  Select just the bones, Click the "Export with Std Mtls" and name the file describing the animation, i.e.: "Fish_jump".  You will be using the same model and bones that you previously exported.

Save as file type ".W3d" and click "Save". When the dialog box pops open, select the "Pure Animation" radio button, and check the box "Export Using Existing Skeleton". Select the name of your bones file, i.e.: "fish_bones.w3d" (if not already selected. Make sure to select the number of frames your animation runs and click "OK" to Export.  

![](images/scrnshots/1r.gif)

That's it, There are other alternate methods for exporting with varying results but this is the method used for most purposes. You should have successfully exported your model, bones and and 2 separate animation's to w3d. Now for the next part, Next we will bring it in to w3d and view it.

***

**END**

[Back to top](#top)