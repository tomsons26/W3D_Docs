###### Getting Started with W3D -The work area >Main Toolbar
# Getting Started with W3D
***
![](images/scrnshots/main_toolbar.gif)

![](images/note.jpg)Some of these buttons may be inactive **(ghosted)**, if they don't apply to the current object or scene.  
Below is a description of what each one does.

***

  

**![](images/scrnshots/newscene_button.gif)**

**New Scene**

**(CTRL + N)** Opens new scene or clears the current scene.

![](images/scrnshots/openscene_button.gif)

**Open**

**(CTRL + O)** Open your files

![](images/scrnshots/ex_emitter_button.gif)

**Export Emitter**

Shortcut to Export an emitter.

![](images/scrnshots/ex_agg_button.gif)

**Export Aggregate**

Shortcut to Export an aggregate.

![](images/scrnshots/ex_lod_button.gif)

**Export LOD**

Shortcut to Export the LOD (Level Of Detail).

![](images/scrnshots/ex_prim_button.gif)

**Export Primitive**

Shortcut to Export a primitive.

![](images/scrnshots/ex_sound_button.gif)

**Export Sound Object**

Shortcut to Export a Sound Object.

![](images/scrnshots/miss_tex_button.gif)

**Missing Textures**

Displays a list of missing textures.

![](images/scrnshots/copy_assets_button.gif)

**Copy Asset Files**

Copies all asset files to a directory you specify.

![](images/scrnshots/add_to_sc_button.gif)

**Add to Scene**

Lets you add new objects to your existing scene. (It will place the new object next to your current object.)

![](images/scrnshots/about_button.gif)

**About**

Displays program number, version and copyright.

  

***

**END**

[Back to top](#top)