###### Getting Started with W3D -The work area >Animation Controller
# Getting Started with W3D, the animation controller
***

![](images/note.jpg)These animation controls, (plus two others- speed and advanced) can also be accessed from the main menu under "Animation". The animation controller does not appear until you have selected an object with animation in it.

  
The Animation controller is a simple floating controller which is used to manipulate the basic animation in the scene.

![](images/scrnshots/anim_cont.gif)

***

**Play**

Plays your animation.

**Stop**

Stops your animation.

**Pause**

Pauses the animation.

**<<**

Moves 1 frame forward.

**>>**

Moves 1 frame Back.

**Close window**

Closes the floater

![](images/idea.jpg)

You can move the floater around by clicking and dragging the title bar. You can even dock the floater in the asset display window and above the status bar by dragging the floater to that area and releasing your mouse button.

![](images/bug.gif)

If you close the animation control Floater, you will not be able to access it again until you close the program and re-open it.

***

**END**

[Back to top](#top)