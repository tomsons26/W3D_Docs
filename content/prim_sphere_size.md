###### Primitives > Sphere > Size
# Primitives - Sphere properties - Size
***
![sphere size](images/scrnshots/prim_sphere_size.gif)  

**Initial Size:**

Set the initial size of the primitive (size is measured in meters)

![](images/scrnshots/local_axis.gif)

**local axis Coordinates:**  
**Z = Height**  
**Y = Width**  
**X = Depth**  
  

**X**

Set the spheres initial diameter depth.

**Y**

Set the spheres initial diameter width.

**Z**

Set the spheres initial diameter height

**Scale:**

The primitive can scale up and down in size by setting the key values at different points in the timeline.

**X-Axis**

Set the spheres diameter depth to scale up and down during it's lifespan.

**Y-Axis**

Set the spheres diameter width to scale up and down during it's lifespan.

**Z-Axis**

Set the spheres diameter height to scale up and down during it's lifespan.

  

***

**END**

[Back to top](#top)