###### Getting Started with W3D -The work area > Main Menu
# Getting Started with W3D
***

**Main Menu**

![](images/scrnshots/main_menu.gif)  
  
**![](images/note_sm.gif)NOTE:** [**Animation**](#animation), [**Hierarchy**](#hierarchy), [**Aggregate**](#aggregate) and [**LOD**](#lod) all occupy the same space on the main menu.  "Animation" will appear only if you have a animation selected, "Hierarchy" will appear only if you have a hierarchy selected, ect. If none of the above is selected from the asset display window, then nothing will appear in that space on the main menu.

***

  

**File**

**New**

**(CTRL + N)** Opens new scene or clears current scene.

**Open**

**(CTRL + O)** Used to open files.

**Munge Sort on Load**

If enabled, will assign static sort levels to a mesh auto.  Required to be set if sorting is turned off.

**Save Settings**

**(CTRL + S)** Lets you save your lighting background and camera settings.

**Load Settings**

Loads your previously saved settings.

**Import Facial Anims**

For Importing Facial animations (.txt files)

**Export**

Lets you export various scene assets.

***

**View**

 

**Toolbars**

Lets you toggle on / off main, object and animation control menus.

**Status Bar**

Lets you toggle on / off the status bar.

**Prev / Next**

**(Pg Up/ Pg Dn)** Scrolls one up / down, through your collection of assets in the asset display window.

**View Fullscreen**

( Disabled )

**Change Device**

( Disabled )

**Change Resolution**

( Disabled )

**Wireframe Mode**

Displays the mesh in your viewport as wireframe.

**Polygon Sorting**

**(CTRL + P)** Toggles Polygon sorting on / off.

**N-Patches Sub Division Level**

Splits Each Triangle into multiple triangles calculated by the selected level squared.  I.E. Level 3, (3 x 3 = 9) 9 triangles

**N-Patches Gap Filling**

This is used for a specialty graphics card, You must have this card in your machine in order for this feature to work. (not widly available yet) the card itself tesselates to add smoothness to a mesh, So low poly meshes instantly become medium res meshes, without taking any load on the cpu.


**Use high-quality shader materials**

Tries to replace old W3D materials with new FX shader materials

**Refresh all**

Reload currently loaded assets

***

**Object**

 

**Rotate X**

**(CTRL + X)** Animates the object rotating around it's"x"axis.

**Rotate Y**

**(Up Arrow)** Animates the object rotating around it's"y"axis.

**Rotate Z**

**(Right Arrow)** Animates the object rotating around it's"z"axis.

**Properties**

Displays the selected asset's properties.

**Restrict Anims**

Restricts a Hierarchy's animation list to those that match it's skeleton.

**Reset**

May Reset an objects animation or tell an emitter to restart from beginning.

**Toggle Alternate Materials**

Toggles between alternate material (If present).

***

**Emitters**

 

**Create Emitter**

Creates a particle effect emitter, for more info see [( Emitters)](emitters.md)

**Scale Emitter**

Lets you uniformly scale the emitter as a whole unit.

**Edit Emitter**

**(Enter)** (with Emitter selected) Edits the selected emitter's properties.

**Edit**

**_Disabled_**

***

**Primitives**

 

**Create Sphere**

Lets you create a sphere and set its properties. See also [(Primitives)](primitives.md)

**Create Ring**

Creates a donut shaped planer right and allows you to set its properties. See also [(Primitives)](primitives.md)

**Edit Primitive**

**(Enter)** (with primitive selected), Allows you to edit your currently selected primitive.

***

**Sound**

**Create Sound Object**

Creates a sound object that can be attached to bones. You may also adjust its properties. See also  
[(Sound)](sound.md).

**Edit Sound Object**

**(Enter)** (with object selected), Allows you to edit your currently selected sound object.

***

**Animation**

 

**Play**

Plays the current animation.

**Pause**

Pauses the current animation.

**Stop**

Stops the current animation.

**Step Back**

Moves back 1 frame of animation.

**Step Forward**

Moves forward 1 frame of animation.

**Speed**

Lets you Adjust the speed of the animation. (Also allows you to toggle on / off the frames blending)

**Advanced**

This opens a window that allows for you to blend two or more animations together.

***

**Hierarchy**

 

**Generate LOD**

Generates an L.O.D. (Level of Detail)

**Make Aggregate**

**(CTRL+A)** Use this to create a new aggregate for more info see: [(Aggregates](aggregates.md))

***

**Aggregate**

 

**Rename Aggregate**

Allows you to rename your aggregate

**Bone Management**

Pops open the bone management box which allows you to attach or remove objects from your bones.

**Auto Assign Bone Models**

This will attempt to find assets, currently loaded in the view, that have the same name as a bone inside the hierarchy. If a match is found, the viewer will attach the object to the bone.

**Bind SubObject LOD**

Makes all Children LOD objects( in the LOD )have the same as the Parent LOD object.

**Generate LOD**

Generates a L.O.D. (Level of Detail)  
LOD models are simply a collection of hierarchical models that can be switched between to meet the current polygon 'budget'. The 'budget' is the number of polygons that can be rendered on a machine in a reasonable frame rate. For faster machines this 'budget' is higher resulting in better looking models. The user can save this LOD by using the "Export" option under the File submenu

***

**LOD**

 

**Record Screen Area**

Use the space bar to set the areas that define where the Sub LOD's will switch. Once you have done this, you must return to max and in the objects properties box, (CTRL + V) to paste the value assigned to that LOD Object. then re-export.

**Include NULL Object**

**?**

**Prev Level**

Cycles back one to the Previous Subobject LOD and i'ts specified Camera Distance.

**Next Level**

Cycles to the Subobject Next LOD and i'ts specified Camera Distance.

**Auto Switching**

When you move your camera closer to- or farther away from the LOD in the viewport, it will switch between LOD sub Objects depending on where you set them.

**Make Aggregate**

**(CTRL+A)** Use this to create a new aggregate for more info see: ([Aggregates](aggregates.md))

***

**Lighting**

 

**Rotate Y**

**(CTRL+Up Arrow)** Toggles (on / off) the light rotating around the mesh objects "Y" Axis.

**Rotate Z**

**(CTRL+Rt Arrow)** Toggles (on / off) the light rotating around the mesh objects "Z" Axis.

**Ambient**

Adjust the RGB / grayscale values of the lighting in your scene.

**Scene Light**

Adjust the advanced lighting properties. See also [( Lighting)](lighting.md).

**Inc Ambient Intensity**

**( \+ )** Increases your Scene's Ambient Light Intensity.  **![](images/minibug.gif): keyboard shortcut disabled.**

**Dec Ambient Intensity**

**( \- )** Decreases your Scene's Ambient Light Intensity.  **![](images/minibug.gif): keyboard shortcut disabled.**

**Inc Ambient Light Intensity**

**(CTRL+"+") ![](images/minibug.gif): keyboard shortcut disabled.**

**Dec Ambient Light Intensity**

**(CTRL+"-") ![](images/minibug.gif): keyboard shortcut disabled.**

**Expose Precalculated Lighting**

**?**

**Kill Scene Light**

**(CTRL+"*")** Sets Scene light's diffuse and specular Channel RGB values to 0,0,0.  
**Note:** this can be turned back on by adjusting the RGB values in the "Scene Light's" properties panel.

**Vertex Lighting**

Toggle between Multi-Pass, Multi Texture and Vertex Lighting for the scene. See also [( Lighting )](lighting.md).

**Multi-Pass Lighting**

For use with the Renegade lightmap tool

**Multi-Texture Lighting**

**?**

***

**Camera**

 

**Front**

**(CTRL + F)** Switches Camera viewpoint to the front of the object.

**Back**

**(CTRL + B)** Switches Camera viewpoint to the back of the object.

**Left**

**(CTRL + L)** Switches Camera viewpoint to the left of the object.

**Right**

**(CTRL + R)** Switches Camera viewpoint to the right of the object.

**Top**

**(CTRL + T)** Switches Camera viewpoint to the top of the object.

**Bottom**

**(CTRL + M)** Switches Camera viewpoint to the bottom of the object.

**Rotate X Only**

Restrict Camera To Rotate on "X" axis only.

**Rotate Y Only**

Restrict Camera To Rotate on "Y" axis only.

**Rotate Z Only**

Restrict Camera To Rotate on "Z" axis only.

**Copy Screen to Clipboard**

**(CTRL + C)** Copy the displayed screen size of the current object to the clipboard.

**Animate Camera**

**( F8 )** **?**

**+X Camera**

Toggles on / off, if the Camera will look down it's x axis.

**Settings**

Adjust the Camera's field of view and clipping planes.

**Set Distance**

Set the Camera distance.

**Reset on Display**

Toggles on / off the Camera Resetting when a new object/asset is selected from the asset display window.

**Reset**

Resets the Camera's orientation back to it's default.

***

**Background**

 

**Color**

Adjust the viewport background color properties.

**Bitmap**

Select a bitmap to be used as a backdrop for the viewport.

**Object**

Select a hierarchy to be used as a background object in the viewport.

**Fog**

**(CTRL + Alt + F)** Toggle fog on / off, in your scene.

***

**Movie**

 

**Make Movie**

Creates a video clip of your animation full frames, uncompressed at the size of your current viewport.

**Capture Screen Shot**

**( F7 )** Captures a screen shot of your current viewport, Stores the screenshot as a .tga, on your local drive in /W3D / W3DVIEW /

  

***

**END**

[Back to top](#top)