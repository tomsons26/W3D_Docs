###### W3D in Max > W3D material(s) > W3D PS2 Shader
# W3D PS2 shader
***
![](images/scrnshots/ps2sha.gif)

The Playstation 2 Shader tab is directly related to the "stage" mapping or (mappers), which is located on the vertex material tab. The way in which these mappers will blend with the background and other objects in the scene will be determined by the blending mode you choose.  
  
Everything in the advanced portion of the shader tab is directly related to the "stage 1 texture" or (detail texture). Use these setting to adjust how the texture behaves if you have enabled stage 1, from the texture tab.

**Blend Mode:**

Select from this variety of ways to blend against the elements of your scene.

**Opaque:**

The map or color in its normal state with No Alpha opacity, and No blended overlay.

**Additive:**

Looks at the color information in each channel and brightens the base color reflecting the colors behind it, creating the effect of an additive overlay. It creates an opacity value based on the grayscale levels of the map.

**Source Substracted:**

Substracts source.

**Destination Substracted:**

Substracts destination.

**Alpha Blend:**

The map or color in its normal state, If the map contains an alpha channel, it uses the alpha channels grayscale values to establish the opacity of the map.

**Alpha Test:**

This will enable / disable itself when certain blend modes are selected.


**Alpha Test and Blend:**

Combines aspects from AlphaTest and Alphablend.

**Custom:**

_You can create a custom blend mode using any of the combinations below;  e.g. If you select the Src to be Alpha, and you select 1 Src Alpha for the Dest, you will have created the "Alpha Blend" effect, however if you used zero for the source and zero for the Dest, this would produce pure black.  
_**![](images/note_sm.gif)NOTE: Many of these custom blend modes aren't very game engine friendly,** use caution when making decisions to use these to avoid costly mistakes.

**(A - B) * C + D**

Output color calculation formula.

**Compatible**

Are the selected blend modes compatible with the output color calculation formula.

**A:**

?

****Source:****

Use source texture as base.

****Destination:****

Use destination texture as base.

****Zero:****

Nothing will be the base.

**B:**

?

****Source:****

Use source texture as base.

****Destination:****

Use destination texture as base.

****Zero:****

Nothing will be the base.

**C:**

?

**Src Alpha:**

Creates a grayscale value based on the color and uses this for the base.

**Dest Alpha:**

Use destination Alpha channel as base.

**One:**

The map in its normal state.

**D:**

?

****Source:****

Use source texture as base.

****Destination:****

Use destination texture as base.

****Zero:****

Nothing will be the base.

**Defaults:**

Clicking this button resets the defaults for the advanced parameters.

**Everything Below here is directly related to the "Stage 1 Texture" which is the secondary (if desired) detail texture.**

**Depth Cmp:**

(Depth Comapre) Tells the graphics card what to do with the polygon its rendering based on the value of the Zbuffer.

**Pass Never:**

New polygons never pass, polygons wont render.

**Pass Less:**

Renders the polygon if the Z value is less than the value thats in the Zbuffer already.

**Pass Always:**

Draw the Polygon no matter what, -ignores the Zbuffer.

**Pass LEqual:**

-_Gets used most often_: Polygon gets drawn only if it's less than or equal to the Zbuffer value.

**Pri Gradient:**

(Primary gradient) This tells it what to do with the diffuse lighting.

**Decal:**

Outputs the texture color as is.

**Modulate:**

Multiplys the color of the pixel by the lighting color.

**Hightlight 1:**

Adjusts the brightness of the texture then adds a hightlight in pure white based on the alpha.
Suitable for highlighting translucent polygons.

**Hightlight 2:**

Similar to **Hightlight 1** but stores alpha  of the texture.
Suitable for highlighting opaque polygons.

**Zbuffer:**

This will enable / disable itself when certain blend modes are selected.

**Alpha Test:**

This will enable / disable itself when certain blend modes are selected.

***
**END**

[Back to top](#top)