###### W3D in Max > Emitters > The W3D Emitter System
# The W3D Emitter System
***
The Emitter System is W3D's own particle system.

It is fully customizable and helps you to create some great special effects.  It designed to be very efficient with game engines,  However, it is ultimately up to the end user to be creative enough to come up with ways to push that efficiency to the max and monitor the load that it takes on the machine.

**To Create a New Emitter:**  
Go to the Main Menu under Emitters >Create Emitter

The Emitter Properties box will pop open.  
![](images/note.jpg)**NOTE:**  Hitting the "OK" button and closing the Emitter Properties box without naming it or changing any properties will make it inaccessible.  
You wont be able to select or edit it.  It is a good idea to name your emitter before clicking the "OK" button.

**![](images/note_sm.gif)ALSO NOTE:**  If you close the file or program, it will not ask you if you'd like to save your work, it will just close the program. You can avoid costly mistakes by ![](images/scrnshots/ex_emitter_button.gif) Exporting often. This saves your emitter so you can reopen it later.  Naming the emitter and exporting it are two different things, make sure to do both.  

**![](images/scrnshots/em_hierarchy.gif)**

![](images/point.jpg)Once you have named it, You can access it through the asset display window in it's hierarchy rollout under "Emitter" just click the name of your emitter to select it.  
  
Once Selected you can edit it's properties by going to the main menu under Emitters > Edit Emitter.  
  
![](images/idea_sm.gif)The easier way to access the properties panel is to select the emitter and hit the "Enter" key on your keyboard.

  

![](images/scrnshots/steam.jpg)

**![](images/note_sm.gif)NOTE:** Most of the emitter's properties are based on a global coordinate system. Certain emitter properties are based on a local system and will be noted. So unless otherwise noted, we are referring to a global coordinate system.

![](images/note_sm.gif)**NOTE:** You can type in negative (-) numerical values in most of the input fields of the emitter properties.  To do so, just type the minus key in before typing a numerical value.

![](images/note_sm.gif)**NOTE:** There is **NO** edit > undo for W3D.  When adjusting the properties of the emitter, It will save you some time to remember the values of what your about to adjust in case you make a mistake you can at least get back to the state at which it previously was.

***

**END**

[Back to top](#top)