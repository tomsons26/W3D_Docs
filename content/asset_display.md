###### Getting Started with W3D -The work area >Asset Display Window
# Getting Started with W3D
***
The Asset Display window displays a list of the different scene objects and assets, categorized in a hierarchy under the type it is. e.g: materials are under the "material" hierarchy. This allows you to select or access them for viewing and editing.  
  
  

![](images/scrnshots/asset_display.gif)

Select the object you would like to view by left clicking on its name. Most objects will appear in the viewport to the right.

Some objects are nested in their perspective hierarchy, in which case you click on the "+" to expand the hierarchy, then select the file you wish to view.

 

  

***

**END**

[Back to top](#top)