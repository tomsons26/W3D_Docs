###### Getting Started with W3D -The work area >Object Controller
# Getting Started with W3D, object controller
***

![](images/note.jpg)Although it is called the "Object controller", most of the controlling is the camera viewing angles. If you accidentally close out the Object controller window, you can reopen it by going to the main menu under "view > toolbars >object".

  
The Object controller helps to give a more precise viewing angle by restricting the camera on one axis so you can move it around on just that axis.

![](images/scrnshots/object_cont.gif)

***

  

![](images/scrnshots/rest_x.gif)

**Rotate Y only**

Restricts camera to only move along the "Y" axis.

![](images/scrnshots/rest_y.gif)

**Rotate X only**

Restricts camera to only move along the "X" axis.

![](images/scrnshots/rest_z.gif)

**Rotate Z only**

Restricts camera to only move along the "Z" axis.

![](images/scrnshots/rot_ob.gif)

**Rotate Object**

Rotates Object around its own "Y" axis.

 

![](images/idea.jpg)

You can move the floater around by clicking and dragging the title bar. You can even dock the floater in the asset display window and above the status bar by dragging the floater to that area and releasing your mouse button.

***

**END**

[Back to top](#top)