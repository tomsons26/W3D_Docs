###### W3D in Max > Emitters
# Emitters
***
#### Overview

**1\. [The W3D Emitter system](em_sys.md)**

**2\. Emitter Properties - [General](emp_gen.md)**

**3\. Emitter Properties - [Emission](emp_em.md)**

**4\. Emitter Properties - [Physics](emp_phy.md)**

**5\. Emitter Properties - [Color](emp_color.md)**

**6\. Emitter Properties - [Size](emp_size.md)**

**7\. Emitter Properties - [User](emp_user.md)**

**8\. Emitter Properties - [Line Properties](emp_lineprop.md)**

**9\. Emitter Properties - [Rotation](emp_rot.md)**

**10\. Emitter Properties - [Frame / Ucoordinate](emp_frameu.md)**

**11\. Emitter Properties - [Line Group](emp_linegroup.md)**  

  

***

**END**

[Back to top](#top)