###### W3D in Max
# W3D in Max
***
#### Overview


The W3D plugin in GMax, actually consists of 5 separate parts found in 2 different areas of the GMax interface.

*   **Create a Renegade Skin:**  Found on the upper right of the GMax Main Toolbar.
*   **Bind to a spacewarp:**  Found on the upper right of the GMax Main Toolbar.
*   **Renegade Material Editor:**  Found on the upper right of the GMax Main Toolbar.
*   **W3D Tools:**  Found under the Utilities tab.
*   **W3D Light Solve:**  Also found under the Utilities tab.

All of these will need to be utilized in order to export files from max to W3D.

***

#### Materials

**1\. [**Accessing the materials**](access_mat.md)**

**2\. [W3D Material Editor Interface](w3d_mated_interface.md)**

**3\. [Vertex Material](vertex_material.md)**

**4\. [W3D Shader](w3d_shader.md)**

**5\. [Textures](m_textures.md)**

***

#### WestWood Skin

**6\. [WWSkin](wwskin.md)**

***

#### W3D Tools

**7\. [W3D tools Exporter](w3d_tools_exporter.md)**

**8\. [Export Options](export_options.md)**

  

***

**END**

[Back to top](#top)