###### Primitives > Ring > Size
# Primitives - Ring properties - Size
*** 
![ring properties](images/scrnshots/prim_ring_size.gif)  
 
**Inner Radii:**

Set the initial size of the inner radius of the ring. (Size is measured in meters)

![](images/scrnshots/local_axis.gif)

**local axis Coordinates:**  
**Z = Height**  
**Y = Width**  
**X = Depth**  

**X**

Set the initial depth of the rings inner radius.

**Y**

Set the initial width of the rings inner radius.

**X-Axis**

Set the local x-axis inner radius (depth) to scale up and down during its lifespan.

**Y-Axis**

Set the local y-axis inner radius (width) to scale up and down during its lifespan.

**Outer Radii:**

**X**

Set the initial depth of the rings outer radius.

**Y**

Set the initial width of the rings outer radius.

**X-Axis**

Set the local x-axis outer radius (depth) to scale up and down during its lifespan.

**Y-Axis**

Set the local x-axis outer radius (width) to scale up and down during its lifespan.


***

**END**

[Back to top](#top)