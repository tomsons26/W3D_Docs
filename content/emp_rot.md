###### W3D in Max > Emitters > The W3D Emitter System > Properties - Rotation
# Emitter Properties - Rotation
***
![](images/scrnshots/em_rotation.gif)  
You can direct the rotational properties of your particles.

  
The left side represents the starting rotation settings (rotations per second) and the right side represents the ending rotation settings.

**To move a key:** click and drag the key left or right on the bar. The beginning Key cannot be moved.  
  
**To edit a keys value:** just double click the key ( ![](images/scrnshots/color_key.gif) ) corresponding to its point in the timeline and type in a numeric value.  Although it doesn't display in the timeline, Negative values are useable and play an important role to getting certain desired effects.

**A Positive numeric value** produces a counter -clockwise rotation

**A Negative numeric value** produces a clockwise rotation.  
  
**To create new keys:** Hold **(CTRL)** while clicking in the timeline window to create new keys and double click them to edit their value. The maximum amount of keys you can set on the timeline is a total of 14 keys.

**To Delete a key:** click the key you wish to delete and hit the "delete" key on your keyboard.

**Velocity Randomizer:**

Increases the velocity of the rotation through time, based on a rotations per second setting.

**Orientation Randomizer:**

Randomizes the starting rotation direction  
Particle orientation is defined using rotations and rotations per second. 1 rotation=360 degrees.

  

***

**END**

[Back to top](#top)