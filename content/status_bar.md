###### Getting Started with W3D -The work area >Status Bar
# Getting Started with W3D
***

The Status bar consists of various information about the scene or selected object(s).

![](images/idea.jpg)

By highlighting certain commands in the [main menu](main_menu.md) or pointing your mouse over some of the [main toolbar](main_toolbar.md) shortcuts, a brief description of that command will appear in the status bar where the word "Ready" is located.  
  
  

***
![](images/scrnshots/status_bar.gif)  

![](images/scrnshots/ready.gif)
Displays the current status of your scene

![](images/scrnshots/polys.gif)
Displays the current amount of polys in your mesh.

![](images/scrnshots/par.gif)
Displays the current amount of particles in your scene.

![](images/scrnshots/cam.gif)
Displays the current Camera distance from center.

![](images/scrnshots/frm.gif)
Displays the scenes current frame number and rate.

![](images/scrnshots/clocks.gif)
How much Cpu power the processor is using,**  
NOTE:** this is used as a "Relative" form of measurement as opposed to an "Absolute" measurement. The processing power is directly related to the machine its currently running on. This is useful when comparing two or more types of scene elements with respect to each other, on the same machine to determine which one is more effecient with the processor.

![](images/scrnshots/dim.gif)
Displays the viewport's current dimensions in pixels.

  

***

**END**

[Back to top](#top)
