###### W3D in Max > Emitters > The W3D Emitter System > Properties - Frame / UCoordinate
# Emitter Properties - Frame / UCoordinate
***
**Frame / UCoordinate:**

![](images/scrnshots/em_frame_ucord.gif)  
The Frame / UCoordinate tab lets you set specific animation and mapping properties for a line group.  
A line group is a bitmap with a set of individual frames arranged from left to right, top to bottom, that the computer reads as a grid, one frame at a time and displays each section of the grid as a single frame. This allows for animated texture mapped particles.

When creating a line group bitmap(aka: grid map), you will need to follow this format:  
1 X 1 (1 frame)  
2 X 2 (2 frames across 2 frames down)  
4 X 4 (4 frames across 4 frames down)  
8 X 8 (8 frames across 8 frames down)  
16 X 16 (16 frames across 16 frames down)  
  
![](images/scrnshots/4x4.gif) ![](images/scrnshots/f_gridmap.jpg)

Above is an example of a "4 X 4" Texture Grid Layout. 4 Frames across and 4 Frames down. The numbers are there to show you how the computer reads your bitmap. The lines are there just to show the separation and aren't necessary in your real bitmap.  
There is no need to separate or draw lines on your bitmap to distinguish between frames, the computer calculates this automatically. As long as your height and width are uniform in size and you specify the texture grid layout appropriately, everything will work fine.  
  
  
You can select the bitmap you wish to use from the [general](emp_gen.md) tab under texture filename.

**To specify which frames will be used and how many times it shall cycle through the grid per particle lifetime:** Use the keys in the display window to control the starting and ending frame numbers throughout the lifetime of the particle. Starting with "0" means you start on frame "1" of the bitmap. After reaching the end number specified, it goes cycles back the beginning number specified. Only utilizing the frame numbers you have specified for it to use. This also directly affects the speed at which, the bitmap will animate. By using a higher number of frames, the bitmap will animate quicker but is relative to the lifetime of the particle.

**To move a key:** click and drag the key left or right on the bar. The beginning Key cannot be moved.  
  
**To edit a keys value:** just double click the key ( ![](images/scrnshots/color_key.gif) ) corresponding to its point in the timeline and type in a numeric value.  Although it doesn't display in the timeline, Negative values are useable and play an important role to the rotation orientation of the particles.  
  
**To create new keys:** Hold **(CTRL)** while clicking in the timeline window to create new keys and double click them to edit their value. The maximum amount of keys you can set on the timeline is a total of 14 keys.

**To Delete a key:** click the key you wish to delete and hit the "delete" key on your keyboard.

**Randomizer:**

Still keeping within the number of frames specified with the keys, it will start on a random frame within the range specified on the randomizer.

**Texture Grid Layout:**

Use this drop down list to select the type of Texture Grid Layout you have.

  

***

**END**

[Back to top](#top)