###### W3D in Max > Emitters > The W3D Emitter System > Properties - Line Properties
# Emitter Properties - Line Properties
***
![](images/scrnshots/em_line_prop.gif)

**Line Rendering options:**

These settings are adjustable only when you have the rendering mode "line" selected under the [general](emp_gen.md) tab.  


**Mapping Mode:**

Adjust this to control the mapping coordinates

**Uniform Width**

Takes the top pixel row of the "U" of the uvw mapping coordinates and draws it down the axis so that it will remain constant throughout the line.

**Uniform Length**

Takes the top pixel row of the "V" of the uvw mapping coordinates and draws it down the axis so that it will remain constant throughout the line.

**Tiled**

this will tile the bitmap

**Merge Intersections:**

When Sub Division is enabled, This will try to elimiate noticably overlapping polys where different parts of the line intersect.

**End Caps:**

**_Disabled:_** Adds a curve to the end of the line.

**Disable Sorting:**

Disables what Sorts the polys back to front from the Z buffer. This will make it faster but less accurate when this is used.

**Parameters:**

**Subdivision Level**

The subdivisions of the line group.

**Noise Amplitude**

Determines the strength of the jitter on a line group.

**Merge Abort Factor:**

**?**

**UVTiling**

How many times the texture is tiled across the line.

**UPerSec**

Animates the mapping moving along the "U" mapping coordinate of the UVW at a per second rate.

**VPerSec**

Animates the mapping moving along the "V" mapping coordinate of the UVW at a per second rate.

  

***

**END**

[Back to top](#top)