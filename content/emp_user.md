###### W3D in Max > Emitters > The W3D Emitter System > Properties - User
# Emitter Properties - User
***
![](images/scrnshots/em_user.gif)

The true intention of this utility is disabled for now, however, it can be used to send the programmer notes through the programmer settings box if desired.  

**Type:**

**_Disabled_**

**Default**

**_Disabled_**

**Programmer Settings:**

You can use this area to give the programmers notes if so desired.

  

***

**END**

[Back to top](#top)