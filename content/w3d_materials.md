###### W3D in Max > W3D materials
# W3D material(s)
***
#### Overview

**1\. [Accessing the materials](access_mat.md)**

**2\. [W3D Material Editor Interface](w3d_mated_interface.md)**

**3\. [Vertex Material](vertex_material.md)**

**4\. [W3D Shader](w3d_shader.md)**

**5\. [W3D PS2 Shader](ps2_shader.md)**

**6\. [Textures](m_textures.md)**  

  

***

**END**

[Back to top](#top)