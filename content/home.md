###### W3D Help
# Home
***

Last Update: October 19, 2001

WARNING! WARNING! WARNING! WARNING! WARNING! WARNING! WARNING!

This document is presented "As Is." It was originally designed as a guide for developers using 3D Studio Max. Many of the menus, terminology, and functions are not available in the GMax tool. This document is simply presented as a guide to understand the W3D tools, NOT as a step by step process for creation of game assets. That said, some lines have been edited to reflect the GMax tool.

***

W3D is a set of set of tools, designed by Westwood Studios, that lets the user prepare (create, convert and view) files to a game developer friendly environment.

A set of 3 plugins work in conjunction with Max, which enables the user to export bones, geometry, animation, and special westwood objects, allowing you to attach dynamic characteristics to them before converting them over to a ".W3D" format.

the W3D viewer allow the user to view the W3D files exported from Max, as well as having the abilities to create particle effects, simple objects, and organize and group together sets of related files.

***