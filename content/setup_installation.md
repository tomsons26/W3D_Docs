###### W3D in Max > Setup and Installation
# Setup and installation of w3d  
***

Follow these steps to setup W3D on your local computer.

![](images/scrnshots/w3dviewicon.gif)First you must locate the root directory of where the W3D zipped file is, in the case of the Las Vegas office, i'ts located here: Mis on 'Cabal' (S)/R&d/w3d/W3DVIEW.zip

  

Double click it to launch winzip (or whatever unzipping utility you use), and extract it to your local directory:  c:\\W3D

![](images/scrnshots/w3dupdateicon.gif)Now, Go back to root folder from where you grabbed the zip file and look for the "W3DUpdate", right click this and choose Create Shortcut from the menu. It wont allow you to create a shorcut there so it will ask you if you want to place it on your desktop. Choose yes to have the shortcut on your desktop. The reason for this is because whenever a newer version comes out, this shortcut will grab the updates from the root directory of where the program is stored rather than your local drive.  
  
First thing you want to do is to update it, so double click the update icon on your desktop and the following dialog box pops up

  

Make sure the paths you use are similar to what is shown in this example:

The viewer goes to your local w3d folder inside the sub folder w3dview,  
**C:\\w3d\\w3dview**

The Max3 Plugin (if you use max3),Goes to your local copy of max3 in the plugins directory under westwood.  
**C:\\3dsmax3\\plugins\\westwood**

The WDump goes in the w3d folder in the sub folder wdump  
**C:\\w3d\\wdump**

The Max4 Plugin (if you use max4),Goes to your local copy of max4 in the plugins directory under westwood.  
**C:\\3dsmax4\\plugins\\westwood**

![](images/scrnshots/w3dupdate.gif)

If you need program elements to be installed in some place other than the default directories, you can click the bars next to the file name to browse for and specify where you want the files installed.  
Clicking the "Defaults" button, sets it back to the original defaults you see above.

The left side of check mark boxes next to the names is for installing them.

The right side of check mark boxes if you wish to "clean" them, or update those of which are already installed.

For first time installation Check mark all left hand side boxes for what you wish to have installed and click "OK".

The Max plugins get installed into their max plugin (westwood) directory and will be available next time you run max.

Once the files finish installing, open the folder located in **C:\\w3d\\w3dview** (or wherever you stored the w3dviewer), and look for the ![](images/scrnshots/w3dviewericon.gif) icon. Right click and create a shorcut.  
Drag the **shortcut** to your desktop. Dragging the Icon itself to your desktop wont work and you'll get an error when you try to launch the program.

![](images/scrnshots/dsktp_w3dview.gif) Thats it, when you want to launch the W3Dviewer, use the shorcut on your desktop.

![](images/scrnshots/dsktop_w3dupdate.gif)When you want to update, use the W3DUpdate shorcut on your desktop.

  

***

**END**

[Back to top](#top)