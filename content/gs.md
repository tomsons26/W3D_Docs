# Getting Started with W3D - The Work Area
***

![](images/scrnshots/interface.gif)

![](images/idea.jpg) Once the viewer is opened, more .w3d files can be added in the viewer by dragging and droping files onto the viewer.

**[Main Menu](main_menu.md)** |     The standard area where most functions in W3D can be accessed from.

**[Main Toolbar](main_toolbar.md)** | Contains a few shortcuts to some of the more often used functions in the program.

**[Asset Display](Asset_display.md)** | Easy access display of your assets in a hierarchical fashion.  
  
**Viewport** | This Displays the objects, lighting and animation in your scene.

**[Status Bar](status_bar.md)** | Displays in real time, various properties in your scene.

**[Animation Controller](anim_cont.md)** | General controls for the basic animation in your scene.

**[Object Controller](object_cont.md)** | Allows your to constrain the rotation axis of the camera as you move around the scene.

**[Aggregates](aggregates.md)** | Aggregates are simply hierarchical models that have render objects tied to their bones.

**[Lighting](lighting.md)** | Allows you to change the lighting in the viewer

![](images/idea.jpg) ( The Animation and Object control floaters can be docked on top of the status or the asset display window).

***

**END**

[Back to top](#top)