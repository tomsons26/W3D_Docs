###### W3D in Max > Emitters > The W3D Emitter System > Properties - Size
# Emitter Properties - Size
***
![](images/scrnshots/em_size.gif)  
You can adjust the size of the particles during their lifetime. The window you see before you represents a timeline in which your particles can grow, shrink or stay the same size according to the value of the keys located at the bottom of the timeline. The colored area's height represents the particles size at that point in time.  
  
The left side represents the beginning of the particles life and the right side represents the end of the particle's life.

**To move a key:** click and drag the key left or right on the bar. The beginning Key cannot be moved.  
  
**To edit a keys value:** just double click the key ( ![](images/scrnshots/color_key.gif) ) corresponding to its point in the timeline. The size of the particle is measured in meters.  
  
**To create new keys:** Hold **(CTRL)** while clicking in the timeline window to create new keys and double click them to edit their size. The maximum amount of size keys you can set on the timeline is a total of 14 keys.

**To Delete a key:** click the key you wish to delete and hit the "delete" key on your keyboard.

**Randomizer**

Selects a random size for your particles to be generated. The randomizer's size value is adjustable in meters. The size of the particles will remain in proportion to the lifespan values that are currently set in the display window.

  

***

**END**

[Back to top](#top)