###### Primitives > Ring > Color
# Primitives - Ring properties - Color
***
![](images/scrnshots/prim_ring_color.gif)

You can adjust the color values of the primitive based on their perspective lifespan. The color bar represents a timeline (left being the beginning and right being the end) in which your primitive will change color over time, according to the value of the keys located at the bottom of the timeline.  
  
For maps: The light grayscale values get colorized based on their lightness, The lighter- the more the color shows, the darker- the less the color shows. Whites get fully colorized while blacks remain black.

**To move a key:** click and drag the key left or right on the bar. The beginning key cannot be moved.  
  
**To edit the color value of a key**: Double click a key ( ![](images/scrnshots/color_key.gif) ) to bring up the color picker for that key. Adjust the color values as you see fit.  
  
**To create new keys:** Hold **(CTRL)** while clicking in the color bar to create new keys and double click them to edit their values. The maximum amount of keys you can set on the timeline is a total of 14 keys per bar.  
  
**To Delete a key:** click the key you wish to delete and hit the "delete" key on your keyboard.

**Opacity:**

You can adjust the transparency of the primitive based on it's perspective lifespan.

  

***

**END**

[Back to top](#top)