W3D alphabetical index  ![](images/leftframe_top.jpg)

**[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#e) [G](#g)**

**[H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n)**

**[O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u)**

**[V](#v) [W](#w) [X](#x) [Y](#y) [Z](#Z)**

***

A

[AABox](w3d_tools_exporter.md) (W3D exporter)

[About (about button)](main_toolbar.md)

[Accessing the materials](access_mat.md)

[Acceleration](emp_phy.md)

[Add](w3d_shader.md) (w3d shader)

[Add Bones](wwskin.md) (WWSkin)

[Add materials](access_mat.md)

[Add to scene](main_toolbar.md)

[Additive](ps2_shader.md) (ps2 shader)

[Additive](emp_gen.md) (emitters general)

[Additive](prim_sphere_gen.md) (primitive sphere)

[Additive](prim_ring_gen.md) (primitive ring)

[Additive Velocity Factors](emp_phy.md)

[Advanced (animation)](main_menu.md)

[Aggregates](aggregates.md) (General info)

[Aggregate](w3d_tools_exporter.md) (W3D exporter)

[Alpha](emp_gen.md) (emitters general)

[Alpha](prim_sphere_gen.md) (primitive sphere)

[Alpha](prim_ring_gen.md) (primitive ring)

[Alpha bitmap](m_textures.md)

[Alpha Blend](w3d_shader.md)  
(w3d shader blending mode)

[Alpha Test](w3d_shader.md) (w3d shader)

[Alpha test](ps2_shader.md) (ps2 shader)

[Alpha test](emp_gen.md) (emitters general)

[Alpha Test and Blend](w3d_shader.md)  
(w3d shader blending mode)

[Alpha test and blend](ps2_shader.md) (ps2 shader)

[Ambient](lighting.md) (light)

[Ambient](vertex_material.md) (vertex material)

[Ambient](main_menu.md) (main menu)

[Animate](wwskin.md)

[Animate Camera](main_menu.md)

[Animation](anim_cont.md) (controller)

[Animation](exp_f_max2.md) (exporting)

[Animation](w3d_tools_exporter.md) (extensions)

[Animation](main_menu.md) (general)

[Animation](vw3d.md) (light)

[Animation](w3d_tools_exporter.md) (material names)

[Animating](exp_f_max.md) (tutorial)

[Applying maps](access_mat.md)

[Args (arguments)](vertex_material.md)

[Asset Dislpay](asset_display.md)

[Assign Extensions](w3d_tools_exporter.md) (w3d exporter)

[Assign Material Names](w3d_tools_exporter.md)  
(w3d exporter)

[Assign Node Names](w3d_tools_exporter.md)  
(w3d exporter)

[Attaching vertices to a bone](wwskin.md)

[Attenuation](lighting.md) (lighting)

[Auto Assign Bone Models](aggregates.md) (aggregates)

[Autogrid](wwskin.md) (WWSkin)

[Back to top](#top)

***

B

[Background object](main_menu.md)

[Background bitmap](main_menu.md)

[Background Fog](main_menu.md)

[Background Color](main_menu.md)

[Base Pose](wwskin.md) (WWSkin)

[Base Texture](m_textures.md)

[Both](lighting.md) (lighting)

[Box](emp_em.md) (emitter shape)

[Box](emp_phy.md) (emitter physics)

[Bone Management](aggregates.md) (Aggregates)

[Bones](exp_f_max.md) (creating, setting up)

[Bones](exp_f_max2.md) (exporting)

[Bones](wwskin.md) (adding to wwskin)

[Bones](wwskin.md) (removing from wwskin)

[Bones](wwskin.md) (displaying in wwskin )

[Bones](wwskin.md) (WWSkin)

[Bind a WWSkin](wwskin.md)

[Bind SubObject LOD](aggregates.md) (Aggregates)

[Bitmap](main_menu.md)

[Blend](w3d_shader.md) (w3d shader)

[Blend mode](ps2_shader.md)

[Bursts](emp_em.md)

[Back to top](#top)

***

C

[Cam- Orient](w3d_tools_exporter.md) (W3D exporter)

[Cam- Parallel](w3d_tools_exporter.md) (W3D exporter)

[Camera](main_menu.md) (controls)

[Camera](status_bar.md) (status bar)

[Camera](w3d_tools_exporter.md) (w3d exporter)

[Camera distance](status_bar.md) (status bar)

[Camera distance](main_menu.md) (set distance)

[Camera aligned](prim_ring_gen.md) (ring primitive)

[Camera aligned](prim_sphere_gen.md) (sphere primitive)

[Camera Settings](main_menu.md)

[Cap](emp_em.md)

[Capture Screen shot](main_menu.md)

[Change Device](main_menu.md)

[Change Resolution](main_menu.md)

[Changing](vw3d.md) (viewing angle)

[Changing](vw3d.md) (lighting position)

[Changing](vw3d.md) (lighting values)

[Clamp U](m_textures.md)

[Clamp V](m_textures.md)

[Classic Environment](vertex_material.md)

[Clocks](status_bar.md)

[Close window](anim_cont.md)

[Cloth Permeable](w3d_mated_interface.md) (surface type)

[Collision Options](w3d_tools_exporter.md)

[Color](emp_color.md) (emitter properties)

[Color](prim_ring_color.md) (ring primitive)

[Color](prim_sphere_color.md) (sphere primitive)

[Concrete](w3d_mated_interface.md) (surface type)

[Controller](anim_cont.md) (animation)

[Controller](object_cont.md) (object)

[Coordinates](emp_frameu.md) (frame/U coordinate)

[Copy Asset Files](main_toolbar.md)

[Copy Screen to Clipboard](main_menu.md)

[Create](aggregates.md) (aggregates)

[Create](exp_f_max.md) (bones in max)

[Create](main_menu.md) (emitter overview)

[Create](em_sys.md) (emitter)

[Create](emp_color.md) (emitter color keys)

[Create](emp_size.md) (emitter size keys)

[Create](emp_rot.md) (emitter rotation keys)

[Create](emp_frameu.md) (frame / Ucoordinate keys)

[Create](emp_linegroup.md) (line group keys)

[Create](exp_f_max.md) (maps, texture maps)

[Create](exp_f_max.md) (model in max)

[Create](primitives.md) (primitives)

[Create](prim_ring_gen.md) (ring)

[Create](w3d_tools_exporter.md) (settings floater)

[Create](prim_ring_color.md) (ring color keys)

[Create](prim_sphere_color.md) (sphere color keys)

[Create](main_menu.md) (sphere general)

[Create](main_menu.md) (sound object)

[Create](main_menu.md) (video Clip)

[Create](wwskin.md) (wwskin)

[Creation Volume](emp_em.md)

[Cylinder](emp_em.md)(emitter emission)

[Cylinder](emp_phy.md)(emiiter physics)

[Custom](ps2_shader.md) (ps2 shader)

[Custom](w3d_shader.md) (w3d shader)

[Back to top](#top)

***

D

[Damage Region](w3d_tools_exporter.md) (W3D exporter)

[Dazzle](w3d_tools_exporter.md) (W3D exporter)

[Dec Ambient Intensity](main_menu.md) (main menu)

[Dec Ambient Intensity](lighting.md) (lighting)

[Dec Ambient light Intensity](lighting.md) (lighting)

[Dec Ambient Light Intensity](main_menu.md) (main menu)

[Decal](ps2_shader.md)

[Default](emp_user.md) (emitter properties)

[Default](ps2_shader.md) (ps2 shader)

[Default](w3d_mated_interface.md) (material editor)

[Default](w3d_shader.md) (w3d shader)

[Depth Cmp](w3d_shader.md) (w3d shader)

[Dest Alpha](ps2_shader.md)

[Dest Cmp](w3d_shader.md)

[Dest Src](w3d_shader.md) (w3d shader)

[Destination](ps2_shader.md)

[Destination Subtractive](ps2_shader.md)

[Detail](w3d_shader.md) (w3d shader)

[Detail Alpha](w3d_shader.md) (w3d shader)

[Detail Blend](w3d_shader.md) (w3d shader)

[Detail color](w3d_shader.md)

[Diffuse](lighting.md) (lighting)

[Diffuse](vertex_material.md) (vertex material)

[Dirt](w3d_mated_interface.md) (surface type)

[Distance](vw3d.md) (camera)

[Disable](emp_lineprop.md) (Sorting)

[Disable](w3d_shader.md) (w3d shader)

[Display](asset_display.md) (asset display window)

[Display](emp_gen.md) (particle blending modes)

[Display](gs.md) (viewport)

[Display](m_textures.md) (textures)

[Dummy boxes](exp_f_max.md)

[Duration](prim_ring_gen.md) (ring)

[Duration](prim_sphere_gen.md) (sphere)

[Back to top](#top)

***

E

[Edit](main_menu.md) (An Emitter)

[Edit](main_menu.md) (A primitive)

[Edit](main_menu.md) (A Sound Object)

[Edit](w3d_mated_interface.md) (materials)

[Electrical](w3d_mated_interface.md) (surface type)

[Electrical Permeable](w3d_mated_interface.md) (surface type)

[Emitters](emitters.md)

[Emitters](em_sys.md) (system)

[Emitters](main_menu.md) (editing, edit)

[Emitters](emp_phy.md) (acceleration)

[Emitters](emp_color.md) (color)

[Emitters](main_menu.md) (creating)

[Emitters](emp_em.md) (Emission)

[Emitters](emp_frameu.md) (Frame/ Ucoordinate)

[Emitters](emp_gen.md) (general properties)

[Emitters](emp_linegroup.md) (line group)

[Emitters](emp_lineprop.md) (line rendering options)

[Emitters](emp_gen.md) (naming)

[Emitters](emp_gen.md) (particle lifetime)

[Emitters](emp_phy.md) (physics)

[Emitters](emp_user.md) (programmer settings)

[Emitters](emp_gen.md) (rendering Modes)

[Emitters](emp_rot.md) (rotation)

[Emitters](emp_gen.md) (shaders)

[Emitters](emp_size.md) (size)

[Emitters](emp_phy.md) (speed factors)

[Emitters](emp_phy.md) (velocity)

[Emitters](emp_user.md) (user)

[Emission](emp_em.md)

[Emissive](vertex_material.md)

[Emissive Lightmap](m_textures.md)

[Enable](w3d_shader.md) (w3d shader)

[End Caps](emp_lineprop.md)

[Environment map](m_textures.md)

[Environment](vertex_material.md) ( vertex material)

[Export with Std Mtls](w3d_tools_exporter.md)

[Exporter](w3d_tools_exporter.md)

[Exporting](aggregates.md) (Aggregates)

[Exporting](w3d_tools_exporter.md) (Animation)

[Exporting](exp_f_max2.md) (bones, tutorial)

[Exporting](main_toolbar.md) (Emitters)

[Exporting](main_menu.md) (files)

[Exporting](w3d_tools_exporter.md) (from MAX)

[Exporting](main_toolbar.md) (from W3D)

[Exporting](w3d_tools_exporter.md) (geometry)

[Exporting](main_toolbar.md) (LOD's)

[Exporting](exp_f_max2.md) (mesh)

[Exporting](main_toolbar.md) (Primitives)

[Exporting](main_toolbar.md) (Sound Objects)

[Exporting](w3d_tools_exporter.md) (transform, bone)

[Expose Precalculated lighting](lighting.md) (lighting)

[Expose Precalculated lighting](main_menu.md) (main menu)

[Back to top](#top)

***

F

[Faq's](faqs.md)

[File](main_menu.md) (general)

[Flammable](w3d_mated_interface.md) (surface type)

[Flammable Permeable](w3d_mated_interface.md) (surface type)

[Flesh](w3d_mated_interface.md) (surface type)

[Fog](main_menu.md) (viewport background)

[Foliage Permeable](w3d_mated_interface.md) (surface type)

[Frame](status_bar.md) (status bar)

[Frame / Ucoordinate](emp_frameu.md)

[Frames](m_textures.md) (textures)

[Frames](exp_f_max2.md) (specifying export amount)

[Front](main_menu.md) (camera view)

[Back to top](#top)

***

G

[Generate LOD](aggregates.md) (Aggregates)

[Geometry Options](w3d_tools_exporter.md) (W3D exporter)

[Glass](w3d_mated_interface.md) (surface type)

[Glass Permeable](w3d_mated_interface.md) (surface type)

[Grass](w3d_mated_interface.md) (surface type)

[Grayscale](lighting.md) (lighting)

[Grid](vertex_material.md) (vertex material)

[Grid Classic Environment](vertex_material.md)  
(vertex material)

[Grid Environment](vertex_material.md) (vertex material)

[Back to top](#top)

***

H

[Heavy Metal](w3d_mated_interface.md) (surface type)

[Help](main_menu.md)

[Height](emp_phy.md) (emitter physics)

[Hide](w3d_tools_exporter.md) (W3D exporter)

[Hierarchy](asset_display.md) (display)

[Highlight](ps2_shader.md) ( ps2 shader)[](m_textures.md)

[Highlight2](ps2_shader.md) ( ps2 shader)[](m_textures.md)

[Hollow](emp_em.md) (emitter emission)

[Hollow](emp_phy.md) (emitter physics)

[Back to top](#top)

***

I

[Ice](w3d_mated_interface.md) (surface type)

[Ice Permeable](w3d_mated_interface.md) (surface type)

[Import Facial Animations](main_menu.md)

[Inc Ambient Intensity](lighting.md) ( lighting)

[Inc Ambient Intensity](main_menu.md) (main menu)

[Inc Ambient light Intensity](lighting.md) (lighting)

[Inc Ambient light Intensity](main_menu.md) (main menu)

[Inheritance](emp_phy.md) (emitter physics)

[Initial Size](prim_sphere_size.md) (sphere primitive)

[Inner Radii](prim_ring_size.md) (ring properties)

[Intensity](lighting.md) (lighting)

[Inv Scale](w3d_shader.md) (w3d shader)

[Invert Effect](prim_sphere_color.md)

[Back to top](#top)

***

J

[Back to top](#top)

***

K

[Kill Scene Light](lighting.md) (lighting)

[Kill Scene Light](main_menu.md) (main menu)

[Back to top](#top)

***

L

[Left](main_menu.md) (camera angle)

[Light Metal](w3d_mated_interface.md) (surface type)

[Lighting](lighting.md) (advanced properties)

[Lighting Distance](lighting.md) (lighting)

[Lighting](main_menu.md) (main menu controls)

[Line](emp_gen.md) (emitters, general)

[Line Group](emp_linegroup.md)

[Line Group bitmap](emp_frameu.md)

[Line Rendering Options](emp_lineprop.md)

[Linear Offset](vertex_material.md) (vertex material)

[Load Settings](main_menu.md)

[Loading Files](vw3d.md) (into W3D)

[Loop](m_textures.md)

[Looping](prim_ring_gen.md) (ring primitive)

[Looping](prim_sphere_gen.md) (sphere primitive)

[Back to top](#top)

***

M

[Main Menu](main_menu.md)

[Main Toolbar](main_toolbar.md)

[Make Movie](main_menu.md)

[Manual](m_textures.md)

[Mappers](vertex_material.md)

[Mapping Mode](emp_lineprop.md) (emitter line properties)

[Material Pass Count](w3d_mated_interface.md) (materials)

[Material](w3d_mated_interface.md) (surface types)

[Material Editor interface](w3d_mated_interface.md) (w3d)

[Max Particles](emp_em.md) (emitter emission)

[Merge Abort Factor](emp_lineprop.md) (emitter line properties)

[Merge Intersections](emp_lineprop.md)

[Missing Textures](main_toolbar.md)

[Modulate](ps2_shader.md) ( ps2 shader)[](m_textures.md)

[Move one frame back](anim_cont.md)

[Move one frame forward](anim_cont.md)

[Movie](main_menu.md) (Make)

[Mud](w3d_mated_interface.md) (surface type)

[Multi-Pass lighting](lighting.md) (lighting)

[Multi-Pass Lighting](main_menu.md) (main menu)

[Multi-Texture lighting](lighting.md) (lighting)

[Multi-Texture Lighting](main_menu.md) (main menu)

[Multiplicative](emp_gen.md) (emitter properties)

[Multiplicative](prim_ring_gen.md) (ring primitive)

[Multiplicative](prim_sphere_gen.md) (Sphere primitive)

[Multiply](w3d_shader.md)  
(w3d shader blending mode)

[Multiply and Add](w3d_shader.md)  
(w3d shader blending mode)

[Munge Sort on Load](main_menu.md)

[Back to top](#top)

***

N

[Name and Color](wwskin.md) (WWSkin)

[Naming Utilities](w3d_tools_exporter.md) (w3d exporter)

[New Scene](main_toolbar.md) (main toolbar)

[N Patches Gap Filling](main_menu.md)

[N Patches Sub Division Level](main_menu.md)

[No LOD](m_textures.md) (textures)

[Noise Amplitude](emp_lineprop.md) (emitter line properties)

[Normal](w3d_tools_exporter.md) (W3D exporter)

[Null (LOD)](w3d_tools_exporter.md) (W3D exporter)

[Back to top](#top)

***

O

[OBBox](w3d_tools_exporter.md) (W3D exporter)

[Object](object_cont.md) (floater Controls)

[Object](gs.md) (controller)

[Object](main_menu.md) (properties)

[Object Export Options](w3d_tools_exporter.md)

[Once](m_textures.md)

[1- Src Alpha](w3d_shader.md) (w3d shader)

[1 -Src Color](w3d_shader.md) (w3d shader)

[One](ps2_shader.md) ( ps2 shader)

[One](w3d_shader.md) (w3d shader)

[Opaque](emp_gen.md) (emitter properties)

[Opaque](prim_ring_gen.md) (ring primitive)

[Opaque](prim_sphere_gen.md) (sphere primitive)

[Opaque](w3d_shader.md)  
(w3d shader blending mode)

[Opacity](prim_ring_color.md) (ring properties)

[Opacity](prim_sphere_color.md) (sphere properties)

[Opacity](vertex_material.md) (vertex material)

[Opacity Vector](prim_sphere_color.md)

[Open](main_toolbar.md)

[Orientation Randomizer](emp_rot.md)

[Outer Radii](prim_ring_size.md)

[Outward](emp_phy.md)

[Back to top](#top)

***

P

[Parameters](emp_lineprop.md) (line properties)

[Particle Lifetime](emp_gen.md)

[Particles](emp_em.md) (emitter emission)

[Particles](status_bar.md) (status bar)

[Pass](w3d_mated_interface.md) (material editor)

[Pass Always](w3d_shader.md) (w3d shader)

[Pass Equal](ps2_shader.md) ( ps2 shader)[](m_textures.md)

[Pass Greater](w3d_shader.md) (w3d shader)

[Pass Hint](m_textures.md)

[Pass LEqual](ps2_shader.md) ( ps2 shader)[](m_textures.md)

[Pass Less](ps2_shader.md) ( ps2 shader)[](m_textures.md)

[Pass Less](w3d_shader.md) (w3d shader)

[Pass GEqual](w3d_shader.md) (w3d shader)

[Pass NEqual](w3d_shader.md) (w3d shader)

[Pass Never](ps2_shader.md) ( ps2 shader)[](m_textures.md)

[Pass Never](w3d_shader.md) (w3d shader)

[Pause](anim_cont.md) (animation controls)

[Phys](w3d_tools_exporter.md) (w3d exporter)

[Physical](w3d_tools_exporter.md) (W3D exporter)

[Ping-Pong](m_textures.md)

[Play](anim_cont.md) (animation controls)

[Polygon Sorting](main_menu.md)

[Polys](status_bar.md) (status bar)

[Prev / Next](main_menu.md)

[Pri Gradient](ps2_shader.md) ( ps2 shader)[](m_textures.md)

[Pri Gradient](w3d_shader.md) (w3d shader)

[Primitives](primitives.md)

[Prism Line Group](emp_gen.md)

[Programmer settings](emp_user.md)

[Proj](w3d_tools_exporter.md) (w3d exporter)

[Projectile](w3d_tools_exporter.md) (W3D exporter)

[Publish](m_textures.md)

[Back to top](#top)

***

Q

[Quad Particles](emp_gen.md)

[Back to top](#top)

***

R

[Ready](status_bar.md) (status bar)

[Rendering Mode](emp_gen.md) (emitters)

[Reset](main_menu.md) (camera)

[Reset on Display](main_menu.md) (camera)

[Restrict Anims](main_menu.md)

[Radius](emp_em.md) (emitter emission)

[Radius](emp_phy.md) (emitter physics)

[Random](vertex_material.md) (vertex material)

[Randomizer](emp_color.md) (emitter system opacity)

[Randomizer](emp_frameu.md) (frame / ucoordinate)

[Randomizer](emp_phy.md) (emitter physics)

[Randomizer](emp_size.md) (emitter size)

[Randomizer Blur Times](emp_linegroup.md)

[Randomizers](emp_color.md) (emitter color)

[Rate](emp_em.md) (emitter emission)

[Rate](m_textures.md) (textures)

[Remove Bones](wwskin.md) (WWSkin)

[Right](main_menu.md) (camera angle)

[Ring](prim_ring_gen.md) (primitive)

[Rock](w3d_mated_interface.md) (surface type)

[Rotate](vertex_material.md) (vertex material)

[Rotate Object](object_cont.md)

[Rotate X](lighting.md) (light)

[Rotate Y](lighting.md) (light)

[Rotate X only](object_cont.md) (object)

[Rotate Y only](object_cont.md) (object)

[Rotate Z only](object_cont.md) (object)

[Rotate X Only](main_menu.md) (camera)

[Rotate Y Only](main_menu.md) (camera)

[Rotate Z Only](main_menu.md) (camera)

[Rotation](emp_rot.md) (emitter rotation)

[Rotation](main_menu.md) (object rotation)

[Rotation](main_menu.md) (lighting)

[Back to top](#top)

***

S

[Sand](w3d_mated_interface.md) (surface type)

[Save Settings](main_menu.md)

[Scale](main_menu.md) (emitter)

[Scale](prim_sphere_size.md) (sphere primitive)

[Scale](vertex_material.md) (vertex material)

[Scale](w3d_shader.md) (w3d shader)

[Scene light](lighting.md) (lighting)

[Scene Light](main_menu.md) (main menu)

[Screen](emp_gen.md) (emitter properties)

[Screen](vertex_material.md) (vertex material)

[Screen](w3d_shader.md)  
(w3d shader blending mode)

[Sec Gradient](w3d_shader.md) (w3d shader)

[Select Alpha Meshes  
](w3d_tools_exporter.md)(w3d exporter)

[Select Bones](w3d_tools_exporter.md) (w3d exporter)

[Select By W3D Type  
](w3d_tools_exporter.md)(w3d exporter)

[Select Geometry](w3d_tools_exporter.md) (w3d exporter)

[Selection Window](w3d_tools_exporter.md) (w3d exporter)

[Shader](emp_gen.md) (emitter general)

[Shader](prim_ring_gen.md) (ring primitive )

[Shader](prim_sphere_gen.md) (Sphere primitive)

[Shadw](w3d_tools_exporter.md) (W3D exporter)

[Shatter](w3d_tools_exporter.md) (W3D exporter)

[Shineyness Mask](m_textures.md)

[Shininess](vertex_material.md) (vertex material)

[Silhouette](vertex_material.md) (vertex material)

[Sine](vertex_material.md) (vertex material)

[Size](emp_size.md) (emitter size)

[Skeleton Parameters](wwskin.md) (WWSkin)

[Snow](w3d_mated_interface.md) (surface type)

[Source](ps2_shader.md) ( ps2 shader)

[Source Subtractive](ps2_shader.md)

[Space Warps](wwskin.md) (WWskin)

[Specular](lighting.md) (lighting)

[Specular](vertex_material.md) (vertex material)

[Specular to Diffuse](vertex_material.md) (vertex material)

[Speed](emp_phy.md) (emitter physics)

[Speed](main_menu.md) (animation)

[Sphere](prim_sphere_gen.md) (primitive)

[Sphere](prim_sphere_gen.md) (primitive properties)

[Sphere](emp_em.md) (emitter shape)

[Sphere](emp_phy.md) (emitter physics)

[Src](w3d_shader.md) (w3d shader)

[Src Alpha](ps2_shader.md) (ps2 shader)

[Src Alpha](w3d_shader.md) (w3d shader)

[Src Color](w3d_shader.md) (w3d shader)

[Src Color Pre Fog](w3d_shader.md) (w3d shader)

[Stage 0 mapping](vertex_material.md) (vertex material)

[Stage 1 mapping](vertex_material.md) (vertex material)

[Stage 0 Texture](m_textures.md)

[Stage 1 Texture](m_textures.md)

[Status Bar](gs.md)

[Step](vertex_material.md) (vertex material)

[Stop](anim_cont.md) (animation)

[Specify](emp_em.md) (creation volume)

[Status bar](status_bar.md)

[Static Sorting Level](w3d_mated_interface.md)

[Steam](w3d_mated_interface.md) (surface type)

[Steam Permeable](w3d_mated_interface.md) (surface type)

[Step Back](main_menu.md)

[Sub](w3d_shader.md) (w3d shader)

[Sub R](w3d_shader.md) (w3d shader)

[Subdivision Level](emp_lineprop.md)

[Back to top](#top)

***

T

[2 Side](w3d_tools_exporter.md) (W3D exporter)

[Toggle Alternate Materials](main_menu.md)

[Toolbars](main_toolbar.md)

[Tetrahedron Line Group](emp_gen.md)

[Texture Filename](emp_gen.md) (emitters)

[Texture Filename](prim_ring_gen.md) (ring primitive)

[Texture Filename](prim_sphere_gen.md) (sphere primitive)

[Texture Tiling](prim_ring_gen.md) (ring primitive)

[Texture grid layout](emp_frameu.md)

[Textures](m_textures.md) (material editor)

[Three Part Plugin](w3d_in_max.md)

[Tiberium Field](w3d_mated_interface.md) (surface type)

[Tiled](emp_lineprop.md) (line properties)

[Top](main_menu.md) (camera angle)

[Translucency](vertex_material.md) (vertex material)

[Triangle Particles](emp_gen.md) (emitters)

[Back to top](#top)

***

U

[Uniform Width](emp_lineprop.md) (line properties)

[Uniform Length](emp_lineprop.md) (line properties)

[UPerSec](emp_lineprop.md) (line properties)

[User](emp_user.md) (Emitter user)

[UV](vertex_material.md) (vertex material)

[UV Channel](vertex_material.md) (vertex material)

[UVTiling](emp_lineprop.md)

[Back to top](#top)

***

V

[VAlpha](w3d_tools_exporter.md) (W3D exporter)

[Vehicle](w3d_tools_exporter.md) (W3D exporter)

[Vertex Lighting](main_menu.md)

[Velocity](emp_phy.md) (emitter physics)

[Vertex lighting](lighting.md) (lighting)

[Vertex Material](vertex_material.md) (material editor)

[View](main_menu.md) FullScreen

[Viewing Files](vw3d.md) (in W3D)

[Viewport](gs.md)

[Vis](w3d_tools_exporter.md) (W3D exporter)

[Volume Randomizer](emp_em.md) (emitter)

[Volume Randomizer](emp_rot.md) (rotation)

[VPerSec](emp_lineprop.md)

[Back to top](#top)

***

W

[W3D](access_mat.md) (accessing material)

[W3D Shader](w3d_shader.md)

[W3D materials](w3d_materials.md) (overview)

[W3D PS2](access_mat.md) (material)

[W3D PS2](ps2_shader.md) (shader)

[W3D Tools Exporter](w3d_tools_exporter.md)

[Water](w3d_mated_interface.md) (surface type)

[Wireframe Mode](main_menu.md)

[Wood](w3d_mated_interface.md) (surface type)

[Work Area](gs.md)

[Write ZBuffer](w3d_shader.md) (w3d shader)

[WS Classic Environment](vertex_material.md)  
(vertex material)

[WS Environment](vertex_material.md) (vertex material)

[WWSkin](wwskin.md)

[Back to top](#top)

***

X

[+X Camera](main_menu.md)

[Back to top](#top)

***

Y

[Back to top](#top)

***

Z

[ZBuffer](ps2_shader.md) ( ps2 shader)[](m_textures.md)

[Zero](ps2_shader.md) (ps2 shader)

[Zero](w3d_shader.md) (w3d shader)

[Zigzag](vertex_material.md) (vertex material)

[ZNrm](w3d_tools_exporter.md) (W3D exporter)

[Back to top](#top)

***