###### W3D in Max > W3D material(s) >Accessing the materials
# W3D Material(s)
***
#### Accessing the W3D materials
To apply maps or add a material to the geometry in your scene, you need to use a special kind of material, used specifically for W3D and found in the material map browser. You can access this material by opening up the material editor and selecting a new swatch and clicking on the button labeled"standard" by default. This will pop open the material / map browser of which you can then select the following material.

![standard](images/scrnshots/standard.gif)  
* **W3D**
***
**END**

[Back to top](#top)