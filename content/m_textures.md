###### W3D in Max > W3D material(s) > Textures
# W3D textures
***

![textures](images/scrnshots/textures.gif)

**Here you can Load bitmaps and adjust their properties.**

**Stage 0 Texture:**

Enable this if you wish to use a bitmap on your mesh.

**Stage 1 Texture (Detail Texture):**

Enable this if you wish to use a secondary bitmap (detail texture)on your mesh.

**None:**

Click this bar to browse and select a bitmap.

**Publish:**

This button instructs the runtime engine to make this a "swappable" texture. You are essentially publishing the existance of this texture to the programmers. This must be enabled for any textures that may need to be swapped or changed at runtime.

**Display:**

This button controls whether the (W3D shader) texture is visible in the Max viewport.  

**Clamp U:**

Nearly Obsolete: For use in help with "T" junctions in geometry, Keeps the map to running across the U coordinate of the geometry in a linear way as to not have minimal seams showing.

**Clamp V:**

Nearly Obsolete: For use in help with "T" junctions in geometry, Keeps the map to running across the U coordinate of the geometry in a linear way as to not have minimal seams showing.

**No LOD:**

Tells it that there there is no L.O.D. associated with this material.

**Frames:**

This spinner can be used to specify that the texture is animated. If you specify a number of frames greater than 1, the texture is assumed to be animated and the other frames for the animation will be generated from the filename you specify for the texture (.tga sequence) . You should use the first frame of the animation as the texture you use in Max.

**Rate:**

Lets you set the frame rate for the frames you will be using, (tga sequence).

**(drop down list):**

For use with Tga sequences.

**Loop:**

Loops the tga sequence from begining to end, then back to beginning, ect.

**Ping-Pong:**

Tga sequence starts from beginning, goes to end, then goes through the frames in reverse, back to begining, ect.

**Once:**

Tga sequence starts from beginning, goes to end, then stops, ends on last file in sequence.

****Manual:****

Manually enter the tga frame for it to cycle through.

**Alpha Bitmap:**

A sort of compression tool for the alpha channel in a bitmap. If enabled, It tells the alpha to either be on or off, reducing it to 1 bit rather than 8.

**Pass Hint:**

These selections do not produce visible results.  It is used to help optimize the code by specifying what type of pass it is so that it doesnt search to see what type of pass it is.

****Base Texture:****

This is the Default and most commonly used. This is just your normal texture.

****Emissive Lightmap:****

This specifies top the code that the texture is basically just a shadow.

****Environment Map:****

This specfies to the code that the texture is basically a reflection map.

****Shineyness Mask:****

This specfies to the code that the texture is basically a shineyness map.

  

***

**END**

[Back to top](#top)