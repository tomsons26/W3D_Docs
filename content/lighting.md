###### W3D in Max > Lighting
# The W3D Lighting
***
**BASIC MOVEMENT:** Lighting can be moved around your scene by holding the CTRL button down on you keyboard while clicking and dragging around the W3D's viewport.  
The light will rotate around the scene object(s). The below Icon, represents the source of the light:  
![](images/scrnshots/light_icon.gif)  
To move the light closer to the objects: Right click and drag downward in the viewport, while holding CTRL down on your keyboard.  
To move the light farther from the objects: Right click and drag upward in the viewport, while holding CTRL down on your keyboard.  
The following commands are located on the [Main Menu](main_menu.md) of the W3D interface.  

**Rotate Y**

**(CTRL+Up Arrow)** Toggles (on / off) the light rotating around the scene's objects "Y" Axis.

![](images/scrnshots/lighting.jpg)**  
NOTICE:** The image to the right is showing the light source coming from the top right, when the true light source is coming from the top left, thats because the object is using the blend mode "Classic Environment". When a model is using the blend mode: Classic Environment, then it gets its specular highlights from the texture map itself, ignoring all scene lighting.

**Rotate Z**

**(CTRL+Rt Arrow)** Toggles (on / off) the light rotating around the scene's objects "Z" Axis.

**Ambient**

Adjust the RGB / grayscale values of the ambient lighting in your scene.  Ambient light is the light affecting all areas in the scene, including shadows and areas the scene light is not necessarily directed towards. The Ambient light properties window pops up. [( For more info: see below)](#ambientlight)

**Scene Light**

Adjust the scene lighting properties. The scene light is the directional light that reflects off the object. The Scene light box pops up. [(For more info: see below)](#scenelight)

**Inc Ambient Intensity**

**( \+ )** Increases your Scene's Ambient Light Intensity.   **( **!** Keyboard shortcut disabled)**

**Dec Ambient Intensity**

**( \- )** Decreases your Scene's Ambient Light Intensity.  ****( **!** Keyboard shortcut disabled)****

**Inc Ambient Light Intensity**

**(CTRL+"+") **( **!** keyboard shortcut disabled)****

**Dec Ambient Light Intensity**

**(CTRL+"-")  **( **!** keyboard shortcut disabled)****

**Expose Precalculated Lighting**

**?**

**Kill Scene Light**

**(CTRL+"*")** Sets Scene light's diffuse and specular Channel RGB values to 0,0,0.  
**Note:** this can be turned back on by adjusting the RGB values in the "Scene Light's" properties panel.

**Vertex Lighting**

Toggle between Multi-Pass, Multi Texture and Vertex Lighting for the scene. See also [( Lighting )](lighting.md).

**Multi-Pass Lighting**

For use with the Renegade lightmap tool

**Multi-Texture Lighting**

**?**

***

**Scene light:**

The scene light is the directional light that reflects off the object.

![](images/scrnshots/scene_light_box.gif)

**Diffuse**

The general area the light reflects off the object.  Select this to adjust the diffuse areas of light.

**Specular**

The highlighted area the light reflects off the object.  Select this to adjust the specular areas of light.

**Both**

Both diffuse and specular areas.  Select this to adjust both the diffuse and specular areas of scene light.

**Red**

Increase or decrease the red levels scene light

**Green**

Increase or decrease the green levels of scene light.

**Blue**

Increase or decrease the blue levels of scene light.

**Grayscale:**

Check this option to uniformly increase or decrease the scene light.

**Intensity:**

Adjust the intensity of the scene light.

**Distance:**

Manually enter the distance for the light to be from the scene's objects.

**Attenuation:**

Adjust the scene light's Attenuation

**Start**

Set the scene light's starting attenuation distance.

**End**

Set the scene light's ending attenuation distance.

***

**Ambient Light:**

Ambient light is the light affecting all areas in the scene, including shadows and areas the scene light is not necessarily directed towards.

![](images/scrnshots/ambient_light.gif)

**Red**

Increase or decrease the red levels of the ambient light.

**Green**

Increase or decrease the green levels of the ambient light.

**Blue**

Increase or decrease the blue levels of the ambient light.

**Grayscale**

Check this option to uniformly increase or decrease the ambient light.

***

**END**

[Back to top](#top)