###### W3D in Max > Emitters > The W3D Emitter System > Properties - Line Group
# Emitter Properties - Line Group
***
![](images/scrnshots/em_linegroup.gif)  
This section allows you to specify blur time keyframes on your line group.  
  
The window represents the particles lifetime.  
The left side represents the start and the right side represents the end of it's life.

**To move a key:** click and drag the key left or right on the bar. The beginning Key cannot be moved.  
  
**To edit a keys value:** just double click the key ( ![](images/scrnshots/color_key.gif) ) corresponding to its point in the timeline and type in a numeric value.  Although it doesn't display in the timeline.

**A Positive numeric value** produces a counter -clockwise rotation

**A Negative numeric value** produces a clockwise rotation.  
  
**To create new keys:** Hold **(CTRL)** while clicking in the timeline window to create new keys and double click them to edit their value. The maximum amount of keys you can set on the timeline is a total of 14 keys.

**To Delete a key:** click the key you wish to delete and hit the "delete" key on your keyboard.

**Randomizer Blur Times:**

Randomizes the length of the tail end of the line group. 0= No tail, 1= 1 Unit.

  

***

**END**

[Back to top](#top)