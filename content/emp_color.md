###### W3D in Max > Emitters > The W3D Emitter System > Properties - Color
# Emitter Properties - Color
***
![](images/scrnshots/em_color.gif)  

You can adjust the color of the particles (and maps) based on their perspective lifespan. The window you see before you represents a timeline (left being the beginning and right being the end) in which your particles will change color over time, according to the color value of the keys located at the bottom of the timeline.  
  
For maps: The light grayscale values get colorized based on their lightness, The lighter- the more the color shows, the darker- the less the color shows. Whites get fully colorized while blacks remain black.

**To move a key:** click and drag the key left or right on the bar. The beginning key cannot be moved.  
  
**To edit the color value of a key**: Double click a key ( ![](images/scrnshots/color_key.gif) ) to bring up the color picker for that key. Adjust the color values as you see fit.  
  
**To create new keys:** Hold **(CTRL)** while clicking in the color timeline to create new keys and double click them to edit their value. The maximum amount of keys you can set on the timeline is a total of 14 keys per bar.  
  
**To Delete a key:** click the key you wish to delete and hit the "delete" key on your keyboard.

**Randomizers:**

Set random values for the overall color.

**R:**

Adjust the Randomizers red values to swing anywhere from 0-255

**G:**

Adjust the Randomizers green values to swing anywhere from 0-255

**B:**

Adjust the Randomizers blue values to swing anywhere from 0-255

**Opacity:**

Adjust the overall opacity of your particles through their perspective lifespan. Works on grayscale values, white values are opaque and Black values are transparent.  
You can set and adjust keys the same as in the main color bar.  
![](images/note_sm.gif) **Note:** Opacity and its randomizer only work while using the "Alpha" Shader, under the [general](emp_gen.md) tab.

**Randomizer:**

Adjust a random percentage of opacity for the particles.

***

**END**

[Back to top](#top)