###### W3D in Max > W3D material(s) > W3D Material Editor Interface
# W3D material editor interface
***
**W3D Material Editor Interface:**

This is the W3D Material editor Interface,

It contains 3 Roll outs It's where you will load your textures and adjust properties of the material. Here is a breakdown of the following elements:

*   [**Material Surface type:**](#matsur)
*   [**Material Pass Count:**](#matpasscnt)
*   [**Pass Rollout:**](#passroll)

**![W3D material Editor Interface](images/scrnshots/w3dmaterial_edit.gif)**

**Material Surface Type**:

The Material Surface type is used in conjunction with an "ini" file which determines which events will occur when objects or projectiles collide with the surface your applying it to. Events vary and may contain any of the following: Decals, emitters, sounds, geometry and animaitons. Heres an example example of an effect of "wood" surface type applied to a box object: A bullet shoots at a wooden crate. The bullet collides with the box, leaving a decal of a bullet hole, an emitter that spits out a few splinters and a sound of a bullet hitting wood occur at the same time. This helps to make the occurence more convincing to the player.  
Use the drop down list to select the type of surface that best fits the physical characteristics of the object your applying it to.

**Light Metal:**

A thin metal, used for metal that may become deformed slightly when hit, e.g. light armor cars and vehicles.

**Heavy Metal:**

A thick metal, used for metal that doesn't get penetrated easily or not at all. e.g. Heavy armor tank.

**Water:**

A liquid, used for creating splash effects when hit. e.g. A lake.

**Sand:**

Sand, e.g. A beach

**Dirt:**

A ground type of surface, e.g A dirt road.

**Mud:**

A muddy surface: used for creating that splashy mud effect. e.g. a mud puddle.

**Grass:**

A grassy surface: e.g. a grassy plain

**Wood:**

A wooden type of surface: e.g. a wooden crate.

**Concrete:**

A concrete surface: e.g. A sidewalk or bricks.

**Flesh:**

A fleshy surface, for use with most mammals, e.g. A human character.

**Rock:**

A rock type of surface. impenatrable and very hard. e.g. a large boulder.

**Snow:**

A snow type of surface: e.g. snow covered mountains.

**Ice:**

An icyy type of surface: e.g. A thin sheet of Ice on a frozen lake.

**Default:**

This is used if you want no collision effects.

**Glass:**

A Glass type of surface, e.g. a window.

**Tiberium Field:**

A Tiberium field type of surface: (for use with command and conquer associated games), e.g. a tiberium field.

**Foliage Permeable:**

A plant line permeable surface, (permeable meaning you can put a hole in it) e.g. A bush

**Glass Permeable:**

A glass permeable surface, (permeable meaning you can put a hole in it) e.g. shatterproof glass.

**Ice Permeable:**

An Ice permeable surface, (permeable meaning you can put a hole in it) e.g. Thick lake Ice.

**Cloth Permeable:**

A cloth permeable surface, (permeable meaning you can put a hole in it) e.g. A blanket hanging on a clothesline.

**Electrical:**

An Electrical surface, used for surfaces that give off a little electric shock when struck. e.g. a transformer.

**Electrical Permeable:**

An Electrical Permeable surface, used for surfaces that electrify when struck. e.g. A television or toaster.

**Flammable:**

A Flammable surface, used for surfaces that become on fire when flame hits them. e.g. A trail of gas.

**Flammable Permeable:**

A Flammable Permeable surface, used for surfaces that burn when caught on fire. e.g. Office papers.

**Steam:**

A steam type of surface. e.g. smoke rising from a chimney.

**Steam Permeable:**

The surface to use when steam can come out of it. e.g. Steam pipes.

**Static Sorting Level:**

Tells the game engine not to sort those polygons. The value you enter into the spinner next to it defines which bactch to render them in.

**Material Pass Count:**

Lets you set the number of passes that your material will use.

 **Pass:**

A single pass is basically the layer which contains the mapper info, texture map, color, ect. A second pass is a second map layer you have the option of adding. E.g: If you have a glass texture with a transparancy value to it on the first pass, you can assign a second pass with a reflection map. The outcome would create a more realistic glass window, however, Using multiple passes comes at a price as it can be costly on the game engines. You have a maximum of 4 passes you can perform.  
**NOTE:** Due to a limitation in our run-time engine, all materials used in a single mesh must have the same number of passes. -If you need to use materials with different numbers of passes, you will have to split the polygons into two separate meshes.  
The pass rollout is broken down into 4 tabs:

**[Vertex Material:](vertex_material.md)**

Lets you set the material (mapper) properties.

**[W3D Shader](w3d_shader.md)**

The original W3D Shader.

**[Textures](m_textures.md)**

Allows you to select various bitmaps and set their properties.

  

***

**END**

[Back to top](#top)